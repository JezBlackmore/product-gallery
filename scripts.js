const images = {
    images: ["https://www.inov-8.com/media/catalog/product/cache/111a8af1e99ed4b2d5fcd19443eb9ee7/0/0/000981-GNGU-M-01-ROCLITE-RECYCLED-GREEN-GUM-1.jpg",
     "https://www.inov-8.com/media/catalog/product/cache/111a8af1e99ed4b2d5fcd19443eb9ee7/0/0/000981-GNGU-M-01-ROCLITE-RECYCLED-GREEN-GUM-3.jpg",
     "https://www.inov-8.com/media/catalog/product/cache/111a8af1e99ed4b2d5fcd19443eb9ee7/0/0/000981-GNGU-M-01-ROCLITE-RECYCLED-GREEN-GUM-4.jpg",
     "https://www.inov-8.com/media/catalog/product/cache/111a8af1e99ed4b2d5fcd19443eb9ee7/0/0/000981-GNGU-M-01-ROCLITE-RECYCLED-GREEN-GUM-6.jpg",
     "https://www.inov-8.com/media/catalog/product/cache/111a8af1e99ed4b2d5fcd19443eb9ee7/0/0/000981-GNGU-M-01-ROCLITE-RECYCLED-GREEN-GUM-7.jpg",
     "https://www.inov-8.com/media/catalog/product/cache/111a8af1e99ed4b2d5fcd19443eb9ee7/0/0/000981-GNGU-M-01-ROCLITE-RECYCLED-GREEN-GUM-1.jpg"],
    lifeStyleImage: "https://inov-8.com/media/345/345header.jpg",
    showAfterElement: 4
}

let showGallery = false;
let childElements = null;
let showAfterElementNumber = images.showAfterElement
let heightOfImages = [];

const productGallery = document.querySelector(".productGalleryContainer .productGallery");
const productGalleryMobile = document.querySelector(".productGalleryContainer__mobile .productGallery");
const productGalleryPopUpImages = document.querySelector('.productGalleryPopUp__images');
const popUp = document.querySelector(".productGalleryPopUp");
const closeButton = document.querySelector(".productGalleryPopUp__close");

const productGalleryFunction = (array) => {
    array.forEach((image, index) => {
        let img = document.createElement('img');
        img.setAttribute("src", `${image}`);

        let img2 = document.createElement('img');
        img2.classList.add('child');
        img2.setAttribute("src", `${image}`);

        productGallery.appendChild(img);
        productGalleryMobile.appendChild(img2);

        let popUpImg = document.createElement('img');
        popUpImg.setAttribute("src", `${image}`);
        popUpImg.classList.add('childSnap');
        productGalleryPopUpImages.appendChild(popUpImg);

  
        heightOfImages.push(popUpImg.height)

        img.onclick = () => {
            showGallery = true;
            displayGallery(array.length, index);
        }
    });

    const fourthChild = document.querySelector(".productGallery").children;
    childElements = fourthChild.length;

    lifeStyleImageFunction(fourthChild);
}

const displayGallery = (arrayLength, index) => {
    if(showGallery){
        popUp.classList.remove("displayNone");

     /*    const productGalleryPopUpImages = document.querySelector('.productGalleryPopUp__images'); */
        const productGalleryPopUpImagesImages = document.querySelectorAll('.productGalleryPopUp__images img');

        let imageArray = Array.from(productGalleryPopUpImagesImages)
   /*  console.log(productGalleryPopUpImages.offsetHeight, productGalleryPopUpImagesImages) */
    const getAllImageheights = imageArray.map(image => {
            return image.offsetHeight
    })

  /*   console.log(getAllImageheights, 78877) */
        const getNeededHeight = getAllImageheights.slice(0, index);
        const scrolLToPosition = getNeededHeight.reduce((acc, cur) => {
            return acc + cur;
        }, 0)
     
        popUp.scrollTo(0, scrolLToPosition); 
        
    } else{
        showGallery = false;
        popUp.classList.add("displayNone");
    }
}
const lifeStyleImageFunction = (elelment) => {
        let img = document.createElement('img');
        img.classList.add('lifeStyle');
        img.setAttribute("src", images.lifeStyleImage);
        elelment.item(4).appendChild(img);
        productGallery.insertBefore(img, elelment.item(showAfterElementNumber));
}

productGalleryFunction(images.images);

closeButton.addEventListener('click', () => {
    showGallery = false;
   
    displayGallery();
})

const chooseWhereToShow = document.querySelector(".chooseWhereToShow");

if(chooseWhereToShow){
chooseWhereToShow.addEventListener("change", (e) => {
  showAfterElementNumber =  e.target.value;
   productGallery.innerHTML = "";
   productGalleryFunction();
})
}


